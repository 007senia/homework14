#include <iostream>
#include <math.h>

class Example
{
private:
	int a = 10;
public:
	void ShowA()
	{
		std::cout << a;
	}
};

class Vector
{
private:
	double x = 0;
	double y = 0;
	double z = 0;
	double l = 0;
public:

	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}

	void Show()
	{
		std::cout << '\n' << x << ' ' << y << ' ' << z;
	}

	void Length()
	{
		l = sqrt(x * x + y * y + z * z);
		std::cout << '\n' << l;
	}
};

int main()
{
	Example temp;
	temp.ShowA();

	Vector v(10,10,10);
	v.Show();
	v.Length();
}